package exeptions;

public class WorngFoodException extends Exception {
    public WorngFoodException() {
    }

    public WorngFoodException(String message) {
        super(message);
    }

    public WorngFoodException(String message, Throwable cause) {
        super(message, cause);
    }

    public WorngFoodException(Throwable cause) {
        super(cause);
    }

    public WorngFoodException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
