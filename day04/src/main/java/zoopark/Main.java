package zoopark;

import animal.*;
import enclosure.Enclosure;
import enclosure.SizeOfEnclosure;
import exeptions.WorngFoodException;
import food.*;

public class Main {
    public static void main(String[] args) throws WorngFoodException {
        Vegan vegan = new Vegan();
        Meat meat = new Meat();

        Animal wolf = new Wolf(SizeOfEnclosure.Middle);
        wolf.name = "Волк";
        wolf.eat(meat);

        Animal leopard = new Leopard(SizeOfEnclosure.Middle);
        leopard.name = "Леопард";
        leopard.eat(meat);


        Animal leo = new Leo(SizeOfEnclosure.Middle);
        leo.name = "Лев";
        leo.eat(meat);

        Animal horse = new Horse(SizeOfEnclosure.Lagre);
        horse.name = "Лошадь";
        horse.eat(vegan);

        Animal elephant = new Elephant(SizeOfEnclosure.Extralagre);
        elephant.name = "Слон";
        elephant.eat(vegan);

        Animal duck = new Duck(SizeOfEnclosure.Small);
        duck.name = "Утка";
        duck.eat(meat);

        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(3, SizeOfEnclosure.Small);

        try {
            enclosureForCarnivorous.animalAdd((Carnivorous) leo);
            enclosureForCarnivorous.animalAdd((Carnivorous) leo);
            enclosureForCarnivorous.animalAdd((Carnivorous)duck);
            enclosureForCarnivorous.animalAdd((Carnivorous)elephant);

        } catch (ClassCastException e) {
            System.out.println("Мне нужен другой вольер!");
        }

        enclosureForCarnivorous.feedAll();

    }
}
