package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.Meat;

public class Wolf extends Carnivorous {
    public Wolf(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public Food getFoodType() {
        return new Meat();
    }
}
