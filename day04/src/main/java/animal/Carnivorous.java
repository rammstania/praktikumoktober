package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.FoodForCarnivorous;


public abstract class Carnivorous extends Animal {
    public Carnivorous(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof FoodForCarnivorous) {
            System.out.println("Я наелся!");
        } else {
            System.out.println("Я такое не ем!");
        }
    }
}



