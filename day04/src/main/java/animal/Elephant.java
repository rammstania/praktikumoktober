package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.Vegan;

public class Elephant extends Herbivore {
    public Elephant(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public Food getFoodType() {
        return new Vegan();
    }

}
