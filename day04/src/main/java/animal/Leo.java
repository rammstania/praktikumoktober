package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.Meat;

public class Leo extends Carnivorous {
    public Leo(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public Food getFoodType() {
        return new Meat();
    }

}
