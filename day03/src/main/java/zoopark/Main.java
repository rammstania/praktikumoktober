package zoopark;

import animal.*;
import enclosure.Enclosure;
import food.*;

public class Main {
    public static void main(String[] args) {
        Vegan vegan = new Vegan();
        Meat meat = new Meat();

//        Animal wolf = new Wolf();
//        wolf.name = "Волк";
//        wolf.eat(meat);
//
//        Animal leopard = new Leopard();
//        leopard.name = "Леопард";
//        leopard.eat(meat);


        Animal leo = new Leo();
        leo.name = "Лев";
        leo.eat(meat);

//        Animal horse = new Horse();
//        horse.name = "Лошадь";
//        horse.eat(vegan);

        Animal elephant = new Elephant();
        elephant.name = "Слон";
        elephant.eat(vegan);

        Animal duck = new Duck();
        duck.name = "Утка";
        duck.eat(meat);

        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(3);

        try {
            enclosureForCarnivorous.animalAdd((Carnivorous) leo);
            enclosureForCarnivorous.animalAdd((Carnivorous) leo);
            enclosureForCarnivorous.animalAdd((Carnivorous)duck);
            enclosureForCarnivorous.animalAdd((Carnivorous)elephant);

        } catch (ClassCastException e) {
            System.out.println("Мне нужен другой вольер!");
        }

        enclosureForCarnivorous.feedAll();

    }
}
