package enclosure;

import animal.Animal;
import java.util.HashMap;
import java.util.Map;

public class Enclosure<T extends Animal> {
    protected int places;

    private HashMap<String, T> enclosure;

    public Enclosure(int size) {
        this.places = size;
        if (size > 0) {
            enclosure = new HashMap<>(size);
        }
    }

    public void animalAdd(T animal) {
        if (places > 0) {
            enclosure.put(animal.name, animal);
            System.out.println("Я " + animal.name + "!" + " Мне очень нравится этот вольер!");
            places--;
        } else {
            System.out.println("Свободных мест нет!");
        }
    }

    public void animalRemove(T animal) {
        enclosure.remove(animal.name);
    }

    public void referenceOfObject(T animal) {
        System.out.println(enclosure.get(animal.name));
    }

    public void feedAll() {
        for (Map.Entry<String, T> entry : enclosure.entrySet()) {
            T value = entry.getValue();
            if (value != null) {
                value.eat(value.getFoodType());
            }
        }
    }
}
