package animal;
import food.Food;
import food.Meat;

public class Wolf extends Carnivorous {
    @Override
    public Food getFoodType() {
        return new Meat();
    }
}
