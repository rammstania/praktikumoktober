package animal;

import food.Food;
import food.FoodForCarnivorous;


public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) {
        if (food instanceof FoodForCarnivorous) {
            System.out.println("Я наелся!");
        } else {
            System.out.println("Я такое не ем!");
        }
    }
}


