package animal;

import food.Food;
import food.FoodForHerbivore;

public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) {
        if (food instanceof FoodForHerbivore) {
            System.out.println("Я наелся!");
        } else {
            System.out.println("Я такое не ем!");
        }
    }

}




