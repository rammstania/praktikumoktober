package animal;

import food.Food;
import interfaces.Feedable;

import java.util.Objects;

public abstract class Animal implements Feedable {
    public String name;

    public abstract void eat(Food food);

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object == null || object.getClass() != this.getClass()) {
            return false;
        }
        Animal animal = (Animal) object;
        return this.name.equals(animal.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
