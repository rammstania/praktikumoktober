package animal;

import food.Food;
import food.Vegan;

public class Elephant extends Herbivore {
    @Override
    public Food getFoodType() {
        return new Vegan();
    }
}
