package animal;

import food.Food;
import food.Meat;

public class Leo extends Carnivorous {
    @Override
    public Food getFoodType() {
        return new Meat();
    }
}
