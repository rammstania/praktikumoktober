package animal;

import food.Food;
import food.Vegan;
import interfaces.Feedable;

public class Horse extends Herbivore implements Feedable {
    @Override
    public Food getFoodType() {
        return new Vegan();
    }
}
