package animal;
import food.Food;
import food.Meat;
import interfaces.Feedable;

public class Leopard extends Carnivorous implements Feedable {
    @Override
    public Food getFoodType() {
        return new Meat();
    }
}
