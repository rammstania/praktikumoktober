package interfaces;

import food.Food;

public interface Feedable {
    public Food getFoodType();
}
