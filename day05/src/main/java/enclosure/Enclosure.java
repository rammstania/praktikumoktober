package enclosure;

import animal.Animal;
import animal.Herbivore;
import exeptions.WorngFoodException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

public class Enclosure<T extends Animal> {
    private static final Logger logger = LoggerFactory.getLogger(Enclosure.class);
    protected int places;
    private HashMap<String, T> enclosure;
    public SizeOfEnclosure sizeOfEnclosureForAnimal;

    public SizeOfEnclosure getSizeOfEnclosureForAnimal() {
        return sizeOfEnclosureForAnimal;
    }

    public Enclosure(int size, SizeOfEnclosure sizeOfEnclosureForAnimal) {
        this.sizeOfEnclosureForAnimal = sizeOfEnclosureForAnimal;
        this.places = size;
        if (size > 0) {
            enclosure = new HashMap<>(size);
        }
    }

    public void animalAdd(T animal) {
        if ((animal.getSizeOfAnimalForEnclosure().getSize() <= this.getSizeOfEnclosureForAnimal().getSize()) && (places > 0)) {
            enclosure.put(animal.name, animal);
            logger.info("Я " + animal.name + "!" + " Мне нравится вольер c размером " + this.getSizeOfEnclosureForAnimal() + "!");
            places--;
        } else if ((animal.getSizeOfAnimalForEnclosure().getSize() != this.getSizeOfEnclosureForAnimal().getSize()) && (places > 0) || animal.getSizeOfAnimalForEnclosure().getSize() > this.getSizeOfEnclosureForAnimal().getSize()) {
            logger.info("Я " + animal.name + "!" + "Мой размер " + animal.getSizeOfAnimalForEnclosure() + "! Мне нужен другой вольер, потому что этот вальер имеет размер " + this.getSizeOfEnclosureForAnimal() + "!");
        } else {
            logger.error("Свободных мест нет!");
        }
    }


    public void animalRemove(T animal) {
        enclosure.remove(animal.name);
    }

    public void referenceOfObject(T animal) {
        System.out.println(enclosure.get(animal.name));
    }

    public void feedAll() throws WorngFoodException {
        for (Map.Entry<String, T> entry : enclosure.entrySet()) {
            T value = entry.getValue();
            if (value != null) {
                value.eat(value.getFoodType());
            }
        }
    }

}
