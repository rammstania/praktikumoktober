package enclosure;

public enum SizeOfEnclosure {
    Small(1) ,
    Middle(2),
    Lagre(3),
    Extralagre(4);

    public int getSize() {
        return size;
    }

    private int size;

    SizeOfEnclosure(int size) {
        this.size = size;
    }
}
