package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.FoodForCarnivorous;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class Carnivorous extends Animal {
    private static final Logger logger = LoggerFactory.getLogger(Carnivorous.class);
    public Carnivorous(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof FoodForCarnivorous) {
            logger.info("Я наелся!");
        } else {
            logger.info("Я такое не ем!");
        }
    }
}



