package animal;
import enclosure.SizeOfEnclosure;
import food.Food;
import food.Meat;
import interfaces.Feedable;

public class Leopard extends Carnivorous implements Feedable {
    public Leopard(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public Food getFoodType() {
        return new Meat();
    }

}
