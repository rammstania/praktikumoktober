package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.Vegan;
import interfaces.Feedable;

public class Horse extends Herbivore implements Feedable {
    public Horse(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public Food getFoodType() {
        return new Vegan();
    }

}
