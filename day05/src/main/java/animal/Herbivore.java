package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.FoodForHerbivore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Herbivore extends Animal {
    private static final Logger logger = LoggerFactory.getLogger(Herbivore.class);
    public Herbivore(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public void eat(Food food) {
        if (food instanceof FoodForHerbivore) {
            logger.info("Я наелся!");
        } else {
            logger.info("Я такое не ем!");
        }
    }

}

