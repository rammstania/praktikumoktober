public class Child extends Mother implements Communication {
    public String name;
    private String gender;
    private int age;

    //геттер/сеттер к имени
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    //геттер/сеттер к полу
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }

    //геттер/сеттер к возрасту
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }

    //конструктор с 1 параметром
    public Child(String gender) {
        this.gender = gender;
    }
    //конструктор с 2 параметрами
    public Child(String gender, String name) {
        this.gender = gender;
        this.name = name;
    }
    //конструктор с 3 параметрами
    public Child(String gender, String name, int age) {
        this.gender = gender;
        this.name = name;
        this.age = age;
    }
    //методы с разными модификаторами
    private void acquaintance(String gender, String name, int age) {
        System.out.println("I was born and my gender is " + gender + ", My name is " + name + ", I'm " + age + " years old!");
    }
    public void myAge(int age) {
        System.out.println("I'm " + age + " year old!");
    }

    //реализация методов интерфейса
    public void food() {
        System.out.println("I'm hungry! (⋟﹏⋞)");
    }

    public void speak() {
        System.out.println("Agu-Agu! (◕‿◕)");
    }

    public void sleep() {
        System.out.println("I want to sleep!" + "(－_－)zzZ");

    }
}
