public class Mother {
    //переменные с разными модификаторами
    public String name;
    private int age;
    private int numberOfChildren;

    //конструктор без параметров
    public Mother() {
    }

    //конструктор с 2 параметрами
    public Mother(String name, int numberOfChildren) {
        this.name = name;
        this.numberOfChildren = numberOfChildren;
    }
    //конструктор с 3 параметрами
    public Mother(String name, int age, int numberOfChildren) {
        this.name = name;
        this.age = age;
        this.numberOfChildren = numberOfChildren;
    }

    //методы с разными модификаторами
    protected void numberOfMyChildren(int numberOfChildren) {
        System.out.println("My name is " + name + " and I have " + numberOfChildren + " children!");
    }

    public void sayHelloYourBaby(String name) {
        System.out.println("Hello, " + Child.class.getName() + " I'm your mother!");
    }
}
