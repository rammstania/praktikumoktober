import org.w3c.dom.ls.LSOutput;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) throws Exception {

        //изменение значений переменных
        //public
        Mother mother = new Mother("Helen", 1);
        System.out.println("Mother's name");
        Class<? extends Mother> motherClass = mother.getClass();
        Field name = motherClass.getField("name");
        System.out.println("Before change:" + name.get(mother));
        name.set(mother, "Lisa");
        System.out.println("After change:" + name.get(mother));
        System.out.println(" ");

        //private
        System.out.println("Number of children");
        Field numberOfChildren = motherClass.getDeclaredField("numberOfChildren");
        numberOfChildren.setAccessible(true);
        System.out.println("Before change:" + numberOfChildren.getInt(mother));
        numberOfChildren.setInt(mother, 2);
        System.out.println("After change:" + numberOfChildren.getInt(mother));
        System.out.println(" ");

        //public
        Child child = new Child("F", "Lola", 1);
        System.out.println("Child's name");
        Class<? extends Child> childClass = child.getClass();
        Field nameOfChild = childClass.getField("name");
        System.out.println("Before change:" + nameOfChild.get(child));
        nameOfChild.set(child, "Luis");
        System.out.println("After change:" + nameOfChild.get(child));
        System.out.println(" ");

        //private
        System.out.println("Child's gender");
        Field gender = childClass.getDeclaredField("gender");
        gender.setAccessible(true);
        System.out.println("Before change:" + gender.get(child));
        gender.set(child, "M");
        System.out.println("After change:" + gender.get(child));
        System.out.println(" ");
        System.out.println("Child's age");
        Field age = childClass.getDeclaredField("age");
        age.setAccessible(true);
        System.out.println("Before change:" + age.getInt(child));
        age.setInt(child, 13);
        System.out.println("After change:" + age.getInt(child));
        System.out.println(" ");

        //получение информации о классе-наследнике
        System.out.println("Getting information about a descendant:");
        Class<Child> clazz0 = Child.class;
        Constructor<?>[] constructors0 = clazz0.getConstructors();
        Method[] methods0 = clazz0.getDeclaredMethods();
        Field[] fields = clazz0.getFields();
        System.out.println("Class Name: " + clazz0.getSimpleName());
        System.out.println("All constructors of the class: " + Arrays.toString(constructors0));
        System.out.println("All methods of the class: " + Arrays.toString(methods0));
        System.out.println("All Fields of the class " + clazz0.getSimpleName() + "is " + Arrays.toString(clazz0.getDeclaredFields()));

        //вызов приватных методов
        Method privateMethod = clazz0.getDeclaredMethod("acquaintance", String.class, String.class, int.class);
        privateMethod.setAccessible(true);
        privateMethod.invoke(child, "F", "Laura", 7);
        System.out.println(" ");

        //создание объекта наследника с помощью рефлексии
        Constructor<Child> constructor = clazz0.getConstructor(String.class, String.class, int.class);
        Child object = constructor.newInstance("M", "Joe", 3);
        System.out.println("New object is: " + "Name: " + object.getName() + ", Gender:" + object.getGender() + ", Age:" + object.getAge());

        //получение информации о суперклассе
        System.out.println("Getting information about the superclass:");
        Class superClazz = clazz0.getSuperclass();
        Constructor<?>[] superConstructors = clazz0.getSuperclass().getConstructors();
        Method[] superMethods = clazz0.getSuperclass().getDeclaredMethods();
        Field[] superFields = clazz0.getSuperclass().getFields();
        System.out.println("Class Name: " + clazz0.getSuperclass());
        System.out.println("All constructors of the class: " + Arrays.toString(superConstructors));
        System.out.println("All methods of the class: " + Arrays.toString(superMethods));
        System.out.println("All Fields of the class " + clazz0.getSuperclass() + "is " + Arrays.toString(clazz0.getSuperclass().getDeclaredFields()));
        System.out.println(" ");

        //получение информации об интерфейсе
        System.out.println("Getting information about the interface:");
        Class interfaces = Communication.class;
        Method[] methodsOfinterfaces = interfaces.getDeclaredMethods();
        Class[] interfacess = clazz0.getInterfaces();
        System.out.println("Class Name: " + interfaces.getSimpleName());
        System.out.println("All methods of the class " + interfaces.getSimpleName() + "is " + Arrays.toString(methodsOfinterfaces));
        //узнаем, имплементирует ли наследник интерфейс
        for (int i = 0, size = interfacess.length; i < size; i++) {
            System.out.print(clazz0.getSimpleName() + " ");
            System.out.print(i == 0 ? "implements " : ", ");
            System.out.print(interfacess[i].getSimpleName());
        }

    }
}
