package animal;
import food.Food;

public abstract class Animal {
    public String name;
    public abstract void eat(Food food);
}
