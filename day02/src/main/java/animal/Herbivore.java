package animal;

import food.Food;
import food.FoodForHerbivore;

public abstract class Herbivore extends Animal {
    @Override
    public void eat(Food food) {
        if (food instanceof FoodForHerbivore) {
            System.out.println("Спасибо, вкусно!");
        } else {
            System.out.println("Я такое не ем!");
        }
    }

    public static class Duck extends Herbivore {
    }

    public static class Elephant extends Herbivore {
    }

    public static class Horse extends Herbivore {
    }
}
