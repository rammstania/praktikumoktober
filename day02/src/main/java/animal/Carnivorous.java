package animal;

import food.Food;
import food.FoodForCarnivorous;

public abstract class Carnivorous extends Animal {
    @Override
    public void eat(Food food) {
        if (food instanceof FoodForCarnivorous) {
            System.out.println("Спасибо, вкусно!");
        } else {
            System.out.println("Я такое не ем!");
        }
    }
    public static class Leo extends Carnivorous {
    }

    public static class Leopard extends Carnivorous {
    }

    public static class Wolf extends Carnivorous {
    }
}
