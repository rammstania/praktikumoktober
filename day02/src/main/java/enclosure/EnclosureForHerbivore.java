package enclosure;

import animal.Animal;
import animal.Herbivore;

public class EnclosureForHerbivore extends Enclosure {
    private Animal[] herbivoreEnclosure;
    private int index;

    public EnclosureForHerbivore(int size) {
        if (size > 0) {
            herbivoreEnclosure = new Animal[size];
        }
    }

    @Override
    public void animalAdd(Animal animal) {
        try {
            if (animal instanceof Herbivore) {
                herbivoreEnclosure[index] = animal;
                System.out.println("Я " + animal.name + "!" + " Мне очень нравится этот вольер!");
                index++;
            } else {
                System.out.println("Я " + animal.name + "!" + " Мне нужен другой вольер!");
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Свободных мест в вольере для травоядных нет!");
        }
    }
}

