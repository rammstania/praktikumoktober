package enclosure;

import animal.Animal;
import animal.Carnivorous;

public class EnclosureForCarnivorous extends Enclosure {
    private Animal[] carnivorousEnclosure;
    private int index;

    public EnclosureForCarnivorous(int size) {
        if (size > 0) {
            carnivorousEnclosure = new Animal[size];
        }
    }

    @Override
    public void animalAdd(Animal animal) {
        try {
            if (animal instanceof Carnivorous) {
                carnivorousEnclosure[index] = animal;
                System.out.println("Я " + animal.name + "!" + " Мне очень нравится этот вольер!");
                index++;
            } else {
                System.out.println("Я " + animal.name + "!" + " Мне нужен другой вольер!");
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Свободных мест в вольере для хищников нет!");
        }
    }
}

