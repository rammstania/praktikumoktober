package enclosure;

import animal.Animal;

public abstract class Enclosure {
    public abstract void animalAdd(Animal animal);
}
