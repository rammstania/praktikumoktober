package zoopark;

import animal.Animal;
import animal.Carnivorous;
import animal.Herbivore;
import enclosure.Enclosure;
import enclosure.EnclosureForCarnivorous;
import enclosure.EnclosureForHerbivore;
import food.Food;
import food.FoodForCarnivorous;
import food.FoodForHerbivore;

public class Main {
    public static void main(String[] args) {
        Food vegan = new FoodForHerbivore.Vegan();
        Food meat = new FoodForCarnivorous.Meat();

        Animal wolf = new Carnivorous.Wolf();
        wolf.name = "Волк";
        wolf.eat(meat);

        Animal leopard = new Carnivorous.Leopard();
        leopard.name = "Леопард";
        leopard.eat(meat);

        Animal leo = new Carnivorous.Leo();
        leo.name = "Лев";
        leo.eat(meat);

        Animal horse = new Herbivore.Horse();
        horse.name = "Лошадь";
        horse.eat(vegan);

        Animal elephant = new Herbivore.Elephant();
        elephant.name = "Слон";
        elephant.eat(vegan);

        Animal duck = new Herbivore.Duck();
        duck.name = "Утка";
        duck.eat(meat);

        Enclosure herbivoreEnclosure = new EnclosureForHerbivore(3);
        Enclosure carnivorousEnclosure = new EnclosureForCarnivorous(3);
        herbivoreEnclosure.animalAdd(duck);
        herbivoreEnclosure.animalAdd(elephant);
        herbivoreEnclosure.animalAdd(horse);
        herbivoreEnclosure.animalAdd(duck);
        carnivorousEnclosure.animalAdd(duck);
        carnivorousEnclosure.animalAdd(leopard);

    }
}
