public class Main {
    static void integralTypes() {
        byte b = 116;
        short s = 1111;
        int i = 64536;
        long l = 2324324342L;
        System.out.println(i);
    }

    static void strings() {
        String a = "Hello", b = "World";
        System.out.println(a + " " + b);

        String c = "" + 3 + 3;
        System.out.println(c);

        String foo = "something";
        String bar = "something";
        String baz = new String("something");

        System.out.println("foo==bar ?" + (foo == bar));
        System.out.println("foo equals bar: " + (foo.equals(bar)));
        System.out.println("foo == baz ? " + (foo == baz));
        System.out.println("foo equals baz ? " + (foo.equals(baz)));
    }

    static void cycleFor() {
        String[] array = new String[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = "step " + i;
            System.out.println(array[i]);
        }
    }

    static void cycleForEach() {
        String string = "массив чаров";
        char[] array = string.toCharArray();
        char letter;
        for (char character : array) {
            letter = character;
            System.out.print(letter);
        }
        System.out.println();
    }

    static void cycleWhile() {
        int[] array = new int[]{1, 3, 5, 7, 9};
        int index = 0;
        try {
            while (index <= array.length) {
                System.out.println(array[index]);
                index++;
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Мы вышли за предел массива на " + array.length + " итерации");
        }
    }

    static void cycleDoWhile() {
        String[] array = new String[]{"Я", "полтора", "часа", "пыталась", "придумать", "новое", "тело", "для", "метода", "знаю", "что", "долго", "пришлось", "копипастнуть"};
        int index = 0;
        do {
            if (array[index].length() >= 4) {
                System.out.println("4 и более букв в массиве содержат слова: " + array[index] + ":(");
            }
            index++;
        }
        while (index < array.length);
    }

    public static void main(String[] args) {
        //integralTypes();
        //strings();

        cycleFor();
        cycleForEach();
        cycleWhile();
        cycleDoWhile();
    }
}
