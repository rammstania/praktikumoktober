package zoopark;

import animal.*;
import enclosure.Enclosure;
import enclosure.SizeOfEnclosure;
import exeptions.WorngFoodException;
import food.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {
    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws WorngFoodException {
        Vegan vegan = new Vegan();
        Meat meat = new Meat();

        Animal wolf = new Wolf(SizeOfEnclosure.Middle);
        wolf.name = "Волк";

        Animal leopard = new Leopard(SizeOfEnclosure.Middle);
        leopard.name = "Леопард";

        Animal leo = new Leo(SizeOfEnclosure.Middle);
        leo.name = "Лев";

        Animal horse = new Horse(SizeOfEnclosure.Lagre);
        horse.name = "Лошадь";

        Animal elephant = new Elephant(SizeOfEnclosure.Extralagre);
        elephant.name = "Слон";

        Animal duck = new Duck(SizeOfEnclosure.Small);
        duck.name = "Утка";

        try {
            wolf.eat(meat);
            leopard.eat(meat);
            leo.eat(meat);
            horse.eat(vegan);
            elephant.eat(vegan);
            duck.eat(meat);
            duck.eat(null);
        } catch (WorngFoodException e) {
            System.out.println("Я такое не ем");
        }

        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(3, SizeOfEnclosure.Middle);

        try {
            enclosureForCarnivorous.animalAdd((Carnivorous) leo);
            enclosureForCarnivorous.animalAdd((Carnivorous) leo);
            enclosureForCarnivorous.animalAdd((Carnivorous) duck);
            enclosureForCarnivorous.animalAdd((Carnivorous) elephant);

        } catch (ClassCastException e) {
            logger.error("Мне нужен другой вольер!");
        }

        enclosureForCarnivorous.feedAll();
    }
}
