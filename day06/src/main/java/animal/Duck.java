package animal;

import enclosure.SizeOfEnclosure;
import food.Food;
import food.Vegan;


public class Duck extends Herbivore {

    public Duck(SizeOfEnclosure sizeOfAnimalForEnclosure) {
        super(sizeOfAnimalForEnclosure);
    }

    @Override
    public Food getFoodType() {
        return new Vegan();
    }
}
