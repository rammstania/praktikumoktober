package animal;

import enclosure.SizeOfEnclosure;
import exeptions.WorngFoodException;
import food.Meat;
import food.Vegan;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class HerbivoreTest {

    @Test
    void testEatVegan() {
        Duck duck = new Duck(SizeOfEnclosure.Small);
        Vegan vegan = new Vegan();
        assertDoesNotThrow(() -> duck.eat(vegan));
    }
    @Test
    void testEatMeat() {
        Duck duck = new Duck(SizeOfEnclosure.Small);
        Meat meat = new Meat();
        assertThrows(WorngFoodException.class, () -> duck.eat(meat));
    }
    @Test
    void testEatNull() throws WorngFoodException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        Duck duck = new Duck(SizeOfEnclosure.Small);
        duck.eat(null);
        String actual = output.toString();
        String expected = "А где еда?";
        assertTrue(actual.contains(expected));
        System.setOut(null);
    }
}