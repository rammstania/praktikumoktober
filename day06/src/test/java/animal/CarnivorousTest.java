package animal;

import enclosure.SizeOfEnclosure;
import exeptions.WorngFoodException;
import food.Meat;
import food.Vegan;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class CarnivorousTest {

    @Test
    void testEatMeat() {
        Leo leo = new Leo(SizeOfEnclosure.Middle);
        Meat meat = new Meat();
        assertDoesNotThrow(() -> leo.eat(meat));
    }
    @Test
    void testEatVegan() {
        Leo leo = new Leo(SizeOfEnclosure.Middle);
        Vegan vegan = new Vegan();
        assertThrows(WorngFoodException.class, () -> leo.eat(vegan));
    }

    @Test
    void testEatNull() throws WorngFoodException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));
        Leo leo = new Leo(SizeOfEnclosure.Middle);
        leo.eat(null);
        String actual = output.toString();
        String expected = "А где еда?";
        assertTrue(actual.contains(expected));
        System.setOut(null);
    }
}