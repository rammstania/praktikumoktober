package enclosure;

import animal.*;
import exeptions.WorngFoodException;
import food.Meat;
import food.Vegan;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import static enclosure.SizeOfEnclosure.*;
import static org.junit.jupiter.api.Assertions.*;

class EnclosureTest {
    private ByteArrayOutputStream output = new ByteArrayOutputStream();

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(output));
    }

    //нужный или мЕньший размер животного
    @Test
    void testAnimalAddWithTheCorrectOrSmallerSize() {
        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(3, Middle);
        Leopard leopard = new Leopard(Small);
        leopard.name = "Леопард";
        enclosureForCarnivorous.animalAdd(leopard);

        String actual = output.toString();
        String expected = "Я " + leopard.name + "!" + " Мне нравится вольер c размером "
                + enclosureForCarnivorous.getSizeOfEnclosureForAnimal() + "!";
        assertTrue(actual.contains(expected));
    }

    //добавление животного бОльшего размера
    @Test
    void testAnimalAddWithTheLargerSize() {
        Enclosure<Herbivore> enclosureForHerbivore = new Enclosure<>(3, Small);
        Elephant elephant = new Elephant(Extralagre);
        elephant.name = "Слон";
        enclosureForHerbivore.animalAdd(elephant);

        String actual = output.toString();
        String expected = "Я " + elephant.name + "!" + "Мой размер " + elephant.getSizeOfAnimalForEnclosure() +
                "! Мне нужен другой вольер, потому что этот вальер имеет размер " + enclosureForHerbivore.getSizeOfEnclosureForAnimal() + "!";
        assertTrue(actual.contains(expected));
    }
    //добавление животного в заполненный вольер
    @Test
    void testAnimalAddToAFullEnclosure() {
        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(1, Middle);
        Leopard leopard = new Leopard(Middle);
        Leo leo = new Leo(Middle);
        enclosureForCarnivorous.animalAdd(leopard);
        enclosureForCarnivorous.animalAdd(leo);
        String actual = output.toString();
        String expected = "Свободных мест нет";
        assertTrue(actual.contains(expected));
    }
    //удаляем животное
    @Test
    void testAnimalRemove() {
        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(1, Middle);
        Leopard leopard = new Leopard(Middle);
        enclosureForCarnivorous.animalAdd(leopard);
        assertTrue(enclosureForCarnivorous.animalRemove(leopard));
    }

    //кормежка
    @Test
    void testFeedAll() throws WorngFoodException {
        Enclosure<Carnivorous> enclosureForCarnivorous = new Enclosure<>(1, Middle);
        Leopard leopard = new Leopard(Middle);
        Vegan vegan = new Vegan();
        Meat meat = new Meat();
        enclosureForCarnivorous.animalAdd(leopard);
        enclosureForCarnivorous.feedAll();
        assertDoesNotThrow(() -> leopard.eat(meat));
        assertThrows(WorngFoodException.class, () -> leopard.eat(vegan));
    }

    @AfterEach
    public void cleanUpStreams() {
        System.setOut(null);
    }
}